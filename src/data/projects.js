export const projects = [
    {
      title: "One Day Menu",
      subtitle: "React, JS, Express, NodeJS",
      description:
        "Created during the pandemic as a proof of concept. Allows anyone to create/host/manage a menu in minutes.",
      image: "./OneDayMenu.png",
      link: "https://onedaymenu.ca/#/menu/onedaymenu",
      orientation: 'left',
    },
    {
      title: "RPG Wordle",
      subtitle: "React and Typescript",
      description:
        "Created during the wordle craze. Endless Wordle with rpg mechanics to spice up the classic game.",
      image: "./KnifeFight.png",
      link: "https://knife-fight.io",
      orientation: 'center',
    },
    {
      title: "Block Challenge",
      subtitle: "React and Typescript",
      description:
        "A block placing game that serves up a unique challenge each day. Race against time to produce the best score.",
      image: "./BlockChallenge.png",
      link: "https://blockchallenge.ca",
      orientation: 'center',
    },
    {
      title: "Automated Crypto Trader",
      subtitle: "AWS, S3, Lambdas, SQS, Javascript, Binance API, Python",
      description:
        "Not good enough to quit my day job but good enough to dedicate time towards it. I created an automated system that collects/analyzes/acts on 300+ crypto currencies. Built on AWS I chain together lambdas(JS) and SQS queues to incrementally look for opportunities. S3 is used for storage and Python is used for backtesting.",
      image: "./Crypto.png",
      link: null,
      orientation: 'left',
    },
    {
        title: "1602 : GridBattle",
        subtitle: "Godot, GDScript(Python)",
        description:
          "A Small game I created using Godot engine. A fantasy sports management game.",
        image: "./GridBattle.png",
        link: "https://jmacattack22.itch.io/1602-gridbattle",
        orientation: 'left',
      },
  ];