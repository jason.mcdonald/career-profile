export const skills = [
    "Javascript - NodeJS, Express",
    "Java",
    "React",
    "Typescript",
    "AWS - S3, Lambdas, SQS, Dashboards",
    "Python",
    "SQL",
    "GraphQL",
    "C#"
  ];