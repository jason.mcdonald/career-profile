import React from "react";

type Props = {
    project: any,
    mobile: boolean
}

const generateImage = (project : any) => {
    return (
        <img
            alt="gallery"
            className={`absolute inset-0 w-full h-full object-cover ${project.orientation === 'center' ? 'object-center' : 'object-left'}`}
            src={project.image}
        />
    )
}

export const ProjectDescription = ({ project, mobile }: Props) =>{

    const [showDescription, setShowDescription] = React.useState(false)

    
    const toggleDescription = () => {
        setShowDescription(showDescription => !showDescription)
    }


    if (mobile) {
        return (
            <button onClick={toggleDescription} className="flex relative">
                {generateImage(project)}
                <div className={`px-8 py-10 relative z-10 w-full border-4 border-gray-800 bg-gray-900 ${showDescription ? ' opacity-100' : ' opacity-0'}`}>
                    <h2 className="tracking-widest text-sm title-font font-medium text-green-400 mb-1">
                        {project.subtitle}
                    </h2>
                    <h1 className="title-font text-lg font-medium text-white mb-3">
                        {project.title}
                    </h1>
                    <p className="leading-relaxed">{project.description}</p>
                    {project.link ?
                        <a
                            href={project.link}
                            key={project.image}> 
                            <button
                                type="button"
                                className="mx-auto mt-8 flex items-center px-5 py-2 border border-transparent text-lg font-medium rounded text-indigo-700 bg-indigo-100 hover:bg-indigo-200 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 select-none"
                            >
                                {"Visit"}
                            </button>
                        </a> : 
                        <></>
                    }
                </div>
            </button>
        )
    }

    return (
        <div className="flex relative">
            {generateImage(project)}
            <div className="px-8 py-10 relative z-10 w-full border-4 border-gray-800 bg-gray-900 opacity-0 hover:opacity-100">
                <h2 className="tracking-widest text-sm title-font font-medium text-green-400 mb-1">
                {project.subtitle}
                </h2>
                <h1 className="title-font text-lg font-medium text-white mb-3">
                {project.title}
                </h1>
                <p className="leading-relaxed">{project.description}</p>
            </div>
        </div>
    );
}