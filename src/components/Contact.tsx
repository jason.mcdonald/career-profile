export const Contact = () =>{
  return (
    <section id="contact" className="relative">
        <div className="bg-gray-900 relative flex flex-wrap py-6 rounded shadow-md container px-5 py-10 mx-auto justify-center">
            <div className="px-6">
                <h2 className="title-font font-semibold text-white tracking-widest text-xs">
                  ADDRESS
                </h2>
                <p className="mt-1">
                  83 Western Island Pond Dr. <br />
                  Torbay, NL, Canada, A1C1M1
                </p>
            </div>
            <div className="px-6 mt-4 lg:mt-0">
                <h2 className="title-font font-semibold text-white tracking-widest text-xs">
                  EMAIL
                </h2>
                <a href="mailto:jason.mcdonald11@gmail.com" className="text-indigo-400 leading-relaxed">
                  jason.mcdonald11@gmail.com
                </a>
                <h2 className="title-font font-semibold text-white tracking-widest text-xs mt-4">
                  PHONE
                </h2>
                <p className="leading-relaxed">902-218-1354</p>
            </div>
        </div>
    </section>
  );
}