import { CodeIcon } from "@heroicons/react/solid";
import { projects } from "../data/projects";

import { ProjectPreview } from "./ProjectPreview";

export const Projects = () =>{
  return (
    <section id="projects" className="text-gray-400 bg-gray-900 body-font">
      <div className="container px-5 py-10 mx-auto text-center lg:px-40">
        <div className="flex flex-col w-full mb-20">
          <CodeIcon className="mx-auto inline-block w-10 mb-4" />
          <h1 className="sm:text-4xl text-3xl font-medium title-font mb-4 text-white">
            Apps I've Built
          </h1>
          <p className="lg:w-2/3 mx-auto leading-relaxed text-base">
            A Collection of hobby projects, game ideas, investment strategies, etc.
          </p>
        </div>
        <div className="flex flex-wrap -m-4">
          {projects.map((project) => (
            <ProjectPreview project={project} key={project.image} />
          ))}
        </div>
      </div>
    </section>
  );
}